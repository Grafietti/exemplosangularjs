angular.module('myApp',[]).controller('namesCtrl', function($scope) {
    $scope.names = [
        {name:'Mario', country:'Portugal'},
        {name:'John' , country:'EUA'},
        {name:'Katsu', country:'Japão'}
    ];
});