/*a variável app obtem o app declado na div, inclui a controller
    que fica responsável por tratar os dados do app
    o tratamento é de iniciar as variáveis de escopo e a função que alterna entre os 
    nome a cada click em h1*/
    var app = angular.module("myApp",[]);
    app.controller('myCtrl', function($scope){
        $scope.firstName = "João";
        $scope.lastName = "Maria";
        $scope.fullName = function(){
            return $scope.firstName + " " + $scope.lastName;
        }    
    });